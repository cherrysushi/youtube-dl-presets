# youtube-dl presets

This bash script helps you use presets in order to easily download your media with youtube-dl

/!\ It cannot function without [youtube-dl](https://github.com/ytdl-org/youtube-dl), so please install it first

## install
First, save this bash script somewhere in your path and allow its execution:

`chmod +x yt`

Then, modify the script to your own liking, notably the download **paths** for audios ([yt](yt#L32) l. 32) and videos ([yt](yt#L35) l. 35)

Please refer to [youtube-dl's own list of options](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#options) and see what they do

## usage
`yt -h` : Displays youtube-dl's own help

`yt -m [URL]` : Downloads mp3 from URL according to presets set inside this script

`yt -v [URL]` : Downloads video from URL according to presets set inside this script

`yt -u` : Updates youtube-dl

`yt -y` : Launches youtube-dl with your own options - don't forget to specify `-o destination/directory`

## termux variant
I wrote a [list of commands](https://gitlab.com/mriutrt/yt-dl-termux) to allow youtube-dl and the presets to work on [termux](https://termux.com/) (android app)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
